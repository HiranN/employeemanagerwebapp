package tech.getarrays.employeemanager.exception;

public class UserNotFoundException extends RuntimeException { //Its gonna call the constructor on the class and pass the message to the constructor
    public UserNotFoundException(String message) {
        super (message);
    }
}
