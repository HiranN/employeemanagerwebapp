//All the attributes that the employee gonna have
//Represents the type of data thats gonna be returning whenever we make those calls to the backend,
//Technically mirroring what an employee looks like from the backend
export interface Employee {
    id: number;
    name: string;
    email: string;
    jobTitle: string;
    phone: string;
    imageUrl: string;
    employeeCode: string;
}