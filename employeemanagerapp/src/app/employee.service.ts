import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Employee } from "./employee";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'  //Let know whole the angular project about the service
})
export class EmployeeService{
    private apiServerUrl = environment.apiBaseUrl;

    constructor(private http: HttpClient) { }

        public getEmployees(): Observable<Employee[]> { //tYpe of data that this request are gonna returning
            return this.http.get<Employee[]>(`${this.apiServerUrl}/employee/all`);
        }

        public addEmployee(employee: Employee): Observable<Employee> { 
            return this.http.post<Employee>(`${this.apiServerUrl}/employee/add`, employee);
        }

        public updateEmployee(employee: Employee): Observable<Employee> { 
            return this.http.put<Employee>(`${this.apiServerUrl}/employee/update`, employee);
        }

        //Whenever we delete an employee we don't send any repsonse back to the response body we 
        //Just send back the whole http response with the status
        public deleteEmployee(employeeId: number): Observable<void> { 
            return this.http.delete<void>(`${this.apiServerUrl}/employee/delete/${employeeId}`);
        }
    }

