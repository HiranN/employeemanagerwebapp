import { Component, OnInit } from '@angular/core';
import { Employee } from './employee';
import { EmployeeService } from './employee.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public employees: Employee[] = [];
  public editEmployee!: Employee | null;
  public deleteEmployee!: Employee; //It represents the delete employee
  

  constructor(private employeeService: EmployeeService) { } //Injected employeeService

  ngOnInit() { //It runs whenever this component is initialized
    this.getEmployees();
  }

  public getEmployees(): void { //Calls the service 
    this.employeeService.getEmployees().subscribe( //we can be notified whenever some data come back from the server or some kind of error
     (Response: Employee[]) =>  {
      this.employees = Response; //It is gonna give whatever response is coming back inside of that body of the response
     },
     (error: HttpErrorResponse) => {
      alert(error.message)
     }
     );
  }

  public onAddEmloyee(addForm: NgForm): void {
    document.getElementById('add-employee-form')?.click();
    //This is the service making call to the back end 
    this.employeeService.addEmployee(addForm.value).subscribe(
      (response: Employee) => {
        console.log(response);
        this.getEmployees();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
        }
    );
  }

  public onUpdateEmloyee(employee: Employee): void {
    
    //This is the service making call to the back end 
    this.employeeService.updateEmployee(employee).subscribe(
      (response: Employee) => {
        console.log(response);
        this.getEmployees();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        }
    );
  }

  public onDeleteEmloyee(employeeId: number): void {
    
    //This is the service making call to the back end 
    this.employeeService.deleteEmployee(employeeId).subscribe(
      (response: void) => {
        console.log(response);
        this.getEmployees();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        }
    );
  }

  public searchEmployees(key: string): void{
    //The constant is used to store all of the employees that match the key
    const results: Employee[] = [];
    for(const employee of this.employees) {
      //means we found it
      if (employee.name.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || employee.email.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || employee.phone.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || employee.jobTitle.toLowerCase().indexOf(key.toLowerCase()) !== -1){ 
        results.push(employee);
      }
      }
      this.employees = results; //represents our new list
      if(results.length === 0 || !key) {
        this.getEmployees(); //it resets this.employees 
      }
  } 

 //Employee that would be either adding,editing or deleting//mode:help to which model to open
  public onOpenModal(employee: Employee | null, mode: string): void{
    const container = document.getElementById('main-container') as HTMLElement;//we have access to the entire div
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';//don't show the button
    button.setAttribute('data-toggle', 'modal');
    if(mode === 'add'){
      button.setAttribute('data-target', '#addEmployeeModal');
    }
    if(mode === 'edit'){
      this.editEmployee = employee;
      button.setAttribute('data-target', '#updateEmployeeModal');
    }
    if(mode === 'delete'){
      if(employee !== null) { //Se employee non è null, contiene qualcosa
      this.deleteEmployee = employee;
      button.setAttribute('data-target', '#deleteEmployeeModal');
      }
    }
    container.appendChild(button);//now the button exist under the DOM
    button.click();//The button will have all the attributes , when click the button it will open the appropriate model
  }

}


//#key -----> local reference 